/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.vertexprise.queueexample;

import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Класс имитирующий отправку запрос на сайт госуслуг
 *
 * @author developer
 */
public class User implements Runnable {

    // Создание блокирующей очереди для отправки сообщений с ответом пользователю
    private final BlockingQueue <Message> guQueue; 
    
    // Создание блокирующей очереди для отправки сообщений на сервер GU    
    private final BlockingQueue<Message> messgeQueue;

    
    // Конструктор класса 
    public User(BlockingQueue<Message> guQueue, BlockingQueue<Message> messgeQueue) {
        this.guQueue = guQueue;
        this.messgeQueue = messgeQueue;
        
         System.out.println("Создан процесс USER");
    }
    
    
    
    
    @Override
    public void run() {

        int count = 0;

        while (true) {

            count++;
            
            // Формирование сообщения с заданием на получение ТК
            Message m = new Message();            
            m.setBody(" Запрос № [" + count + "] на получение трудовой книжки ...");

            try {

                System.out.println("[USER] Пользователь отправил сообщение [" + count + "]");
                // Размещение сообщения в очереди для отправки на сайт Госулуг
                messgeQueue.put(m);

                // Прием ответа из Госуслуг
                System.out.println("[USER] Ожидаю ответа от Госуслуг ...");
                Message message = guQueue.take();
                
                System.out.println("-------------------------");
                System.out.println("Получен ответ из ГУ:");
                System.out.println(message.getBody());
                System.out.println("-------------------------");

            } catch (InterruptedException ex) {
                System.out.println("Ошибка отправки: " + ex.toString());;
            }

            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

}
