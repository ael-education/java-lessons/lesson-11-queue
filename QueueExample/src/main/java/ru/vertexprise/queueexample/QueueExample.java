/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package ru.vertexprise.queueexample;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author developer
 */
public class QueueExample {

    public static void main(String[] args) {
        
        ExecutorService executor = Executors.newFixedThreadPool(2);
       
        // Создание блокирующей очереди для отправки сообщений на сервер GU
        BlockingQueue <Message> messgeQueue = new ArrayBlockingQueue<>(10);        
        
        // Создание блокирующей очереди для отправки сообщений с ответом пользователю
        BlockingQueue <Message> guQueue = new ArrayBlockingQueue<>(10);
        
        
        User user = new User(guQueue, messgeQueue);
        ClusterGU clusterGU = new ClusterGU(guQueue, messgeQueue);
        
       
        System.out.println("Запуск имитации работы USER");
        executor.submit(user);
        executor.submit(clusterGU);
        
        
    }
}
