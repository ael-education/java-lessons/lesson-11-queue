/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.vertexprise.queueexample;

import java.util.concurrent.BlockingQueue;

/**
 *
 * @author developer
 */
public class ClusterGU implements Runnable{
 
     // Создание блокирующей очереди для отправки сообщений с ответом пользователю
    private final BlockingQueue <Message> guQueue; 
    
    // Создание блокирующей очереди для отправки сообщений на сервер GU    
    private final BlockingQueue<Message> messgeQueue;

    
    // Конструктор класса для имитации Госуслуг
    public ClusterGU(BlockingQueue<Message> guQueue, BlockingQueue<Message> messgeQueue) {
        this.guQueue = guQueue;
        this.messgeQueue = messgeQueue;
    }

    
    @Override
    public void run() {
        System.out.println("[ClusterGU] Создан имитатор сайта Госуслуги");
        while (true) {
            System.out.println("[ClusterGU] Ожидаю запроса от USER ...");

            try {

                // Прием запроса из очереди от USER
                Message message = messgeQueue.take();

                System.out.println("[ClusterGU] Извлекаю задание ...");
                Thread.sleep(500);
                System.out.println("[ClusterGU] Извлек задание: [" + message.getBody() + "]");
                System.out.println("[ClusterGU] Работаю над заданием ....");
                Thread.sleep(2000);

                message.setBody("ответ: ТРУДОВАЯ КНИЖКА [USER]");
                guQueue.put(message);

                System.out.println("[" + ClusterGU.class.getSimpleName() + "] Задание выполнено - ответ отправлен");

            } catch (InterruptedException ex) {
                System.out.println("[ClusterGU ] Ошибка приема: " + ex.toString());
            }

        }

    }
       
    
    
}
